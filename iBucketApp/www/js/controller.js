angular.module('starter.controllers', ['firebase'])

    .controller('HomeCtrl',['$scope','$firebaseAuth','$state', '$ionicPopup', function($scope,$firebaseAuth,$state,$ionicPopup) {

		$scope.login={};
		var firebaseObj = new Firebase("https://burning-fire-7665.firebaseio.com");
		$scope.register = function() {
		    var username = $scope.login.username;
		    var password = $scope.login.password;

		    firebaseObj.createUser({
			    email : username,
				password : password
				}, function(error, userData) {
			    if (error) {
				console.log("Error creating user:", error);
			    } else {
				console.log("Successfully created user account");
				$state.go('registerHome');
			    }
			})}

		$scope.signin = function(){
		    var username = $scope.login.username;
		    var password = $scope.login.password;

		    firebaseObj.authWithPassword({
			    email: username,
				password: password
				},
		    function(error, authData) {
		        if (error) {
			    console.log("Login error:", error);
			}
			else {
			    console.log("Authentication successful:", authData);
			    $state.go('userHome');
			}
		    })
		}
	    }])
		

    .controller('UserHomeCtrl', ['$scope', function($scope){
    
	    }])

    .controller('RegisterHomeCtrl', ['$scope', function($scope) {
	    }])